import subprocess

from setuptools import setup

VERSION = subprocess.check_output(["git", "describe", "--tags", "--dirty"]).strip().decode('utf-8')

def readme():
    """get content of the README.md file"""
    with open("README.md", "r", encoding="utf-8") as readme_file:
        return readme_file.read()

setup(
    name='rpi-tm1637',
    py_modules=['tm1637'],
    version=str(VERSION),
    description='Raspberry Pi Python port from MicroPython library for TM1637 LED driver.',
    long_description=readme(),
    long_description_content_type="text/markdown",
    keywords='tm1637 raspberry pi seven segment led python',
    url='https://gitlab.com/lookslikematrix/rpi-tm1637',
    author='Mike Causer',
    author_email='mcauser@gmail.com',
    maintainer="Christian Decker",
    maintainer_email="christian.decker@lookslikematrix.de",
    license='MIT',
    classifiers=[
        'Programming Language :: Python :: 3',
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
    ],
    python_requires='>=3',
    install_requires=['wiringpi']
)
